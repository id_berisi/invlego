-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 28 Okt 2016 pada 20.27
-- Versi Server: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `invlego`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_lego`
--

CREATE TABLE `data_lego` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `merek` int(11) NOT NULL,
  `seri` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `warna` varchar(7) NOT NULL,
  `tanggal_beli` date NOT NULL,
  `url_photo` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_lego`
--

INSERT INTO `data_lego` (`id`, `nama`, `merek`, `seri`, `type`, `jumlah`, `warna`, `tanggal_beli`, `url_photo`) VALUES
(15, 'Flame Trooper', 2, 1, 0, 4, '#ffffff', '2016-05-31', 'flametrooper.jpg'),
(16, 'Scout Trooper', 3, 1, 0, 1, '#ffffff', '2014-12-31', 'scouttrooper.jpg'),
(18, 'Sand Trooper', 4, 1, 0, 2, '#ffffff', '2016-07-31', 'sandtrooper.jpg'),
(19, 'Clone Trooper', 3, 1, 0, 1, '#ffffff', '2015-01-31', 'clonetrooper.jpg'),
(20, 'Imperial Guard', 3, 1, 0, 1, '#ff0000', '2015-01-31', 'redarmi.jpg'),
(22, 'Comander Billy', 3, 1, 0, 1, '#ffff00', '2015-01-31', 'comanderbily.jpg'),
(23, 'Comander Ponds', 3, 1, 0, 1, '#800000', '2015-01-31', 'comanderponds.jpg'),
(24, 'Bom Squad Clone Trooper', 3, 1, 0, 1, '#ff8000', '2015-01-31', 'orangeclonetrooper.JPG'),
(25, 'Green Scout Trooper', 3, 1, 0, 1, '#008000', '2015-01-31', 'greenscouttrooper.jpg'),
(26, 'Commander Gree', 3, 1, 0, 1, '#008000', '2015-01-31', 'comandergree.jpg'),
(27, 'Captain Pasma', 4, 1, 0, 1, '#808080', '2016-07-31', 'captainpasma.jpg'),
(28, 'Tie Pilot', 4, 1, 0, 3, '#000000', '2016-07-31', 'tiepilot.JPG'),
(29, 'Spiderman', 3, 2, 0, 1, '#ff0000', '2016-07-31', 'spiderman.jpg'),
(32, 'Charde Vincent', 0, 0, 0, 26, '#d34839', '1990-01-31', 'Jada Duffy'),
(33, 'Alexis Wright', 0, 0, 0, 48, '#7adbd5', '2007-10-31', 'Jena Little'),
(34, 'Minerva Fernandez', 0, 0, 0, 24, '#bab5ab', '1993-10-23', 'Channing Flynn'),
(35, 'Noble Dale', 0, 0, 0, 31, '#7232de', '2013-08-26', 'tiepilot.JPG'),
(36, 'Morgan Murray', 0, 0, 0, 44, '#83477e', '2011-01-23', 'Zenia Palmer'),
(37, 'Mallory Sharpe', 0, 0, 0, 97, '#04108a', '2008-11-23', 'Erich James'),
(39, 'Captain America', 2, 2, 0, 1, '#0000ff', '2014-01-01', 'captainamerica.jpg'),
(40, 'First Order', 2, 1, 0, 1, '#ffffff', '2016-07-31', 'firstorder.jpg'),
(41, 'Iron Man', 2, 2, 0, 1, '#ff0000', '2016-07-31', 'ironman.jpg'),
(43, 'Black Spiderman', 3, 3, 0, 1, '#000000', '2016-07-31', 'blackspiderman.jpg'),
(44, 'Red Black Spiderman', 3, 3, 0, 1, '#ff0000', '2016-07-31', 'blackredspiderman.jpg'),
(45, 'Iron Patriot', 3, 2, 0, 1, '#808080', '2016-07-31', 'ironpatriot.jpg'),
(46, 'Ben', 2, 5, 0, 1, '#ff8000', '2016-05-31', 'ben.jpg'),
(47, 'Rabbit', 2, 4, 0, 1, '#ffffff', '2016-05-31', 'rabbit.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_merek`
--

CREATE TABLE `data_merek` (
  `merek_id` int(11) NOT NULL,
  `nama_merek` varchar(50) NOT NULL,
  `poto_merek` text NOT NULL,
  `keterangan_merek` text NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_merek`
--

INSERT INTO `data_merek` (`merek_id`, `nama_merek`, `poto_merek`, `keterangan_merek`, `user_id`) VALUES
(1, 'Lego', '', 'Original Lego', 0),
(2, 'Shenyuang', '', 'Shenyuang', 0),
(3, 'Bela', '', 'Bela', 0),
(4, 'Lepin', '', 'Lepin', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_seri`
--

CREATE TABLE `data_seri` (
  `seri_id` int(11) NOT NULL,
  `nama_seri` varchar(50) NOT NULL,
  `poto_seri` text NOT NULL,
  `keterangan_seri` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_seri`
--

INSERT INTO `data_seri` (`seri_id`, `nama_seri`, `poto_seri`, `keterangan_seri`) VALUES
(1, 'Starwars', '', ''),
(2, 'Marvel Super Hero', '', ''),
(3, 'Marvel Villian', '', ''),
(4, 'Animalia', '', 'Animal Minifigure'),
(5, 'Fantastik Four', '', 'Fantastic Four');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_type`
--

CREATE TABLE `data_type` (
  `type_id` int(11) NOT NULL,
  `nama_type` varchar(50) NOT NULL,
  `keterangan_type` text NOT NULL,
  `photo_type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_lego`
--
ALTER TABLE `data_lego`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_merek`
--
ALTER TABLE `data_merek`
  ADD PRIMARY KEY (`merek_id`);

--
-- Indexes for table `data_seri`
--
ALTER TABLE `data_seri`
  ADD PRIMARY KEY (`seri_id`);

--
-- Indexes for table `data_type`
--
ALTER TABLE `data_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_lego`
--
ALTER TABLE `data_lego`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `data_merek`
--
ALTER TABLE `data_merek`
  MODIFY `merek_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `data_seri`
--
ALTER TABLE `data_seri`
  MODIFY `seri_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `data_type`
--
ALTER TABLE `data_type`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
