<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/pace.min.js"></script>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="css/pace.css" rel="stylesheet" />
</head>
<script src="js/angular.min.js"></script>
<body>
	<div class="container" ng-app="myApp" ng-controller="customersCtrl">
		<div class="page-header">
		  <h1>LEGOINV <i data-toggle="modal" data-target="#myModal" class="fa fa-plus fa-1x"></i></h1> 
		  <div class="row">
			<div class="panel btn-group col-md-12">
				<button class="btn btn-primary" type="button" ng-repeat="x in dStatistik">
				  {{ x.nama_seri }} <span class="badge">{{ x.count}}</span> 
				</button>
			</div>
			<div class="panel btn-group col-md-12">
				<button class="btn btn-success" type="button" ng-repeat="x in dStatistikMerek">
				  {{ x.nama_merek }} <span class="badge">{{ x.count}}</span> 
				</button>
			  </div>
		  </div>
		  <div class="row">
			
		  </div>
		  
		  
		</div>
	
			<div>
				<div class="row">
				  <div class="col-md-3" ng-repeat="x in myData">
					<div class="panel panel-default">
					  <div class="panel-heading">{{ x.nama }} <i class="fa fa-times" aria-hidden="true" ng-click="deleteLego(x.id)"></i> </div>
					  <div class="panel-body">
						<center><img class='imgtmb' src='../uploads/{{ x.url_photo }}'/></center>
						<table class="table">
							<tr><td>Merk</td><td>{{ x.nama_merek }}</td><tr>
							<tr><td>Series</td><td>{{ x.nama_seri }}</td><tr>
							<tr><td>Buy Date</td><td>{{ x.tanggal_beli }}</td><tr>
							<tr><td>Main Color</td><td><span class="glyphicon glyphicon-stop" style="color:{{ x.warna }}" aria-hidden="true"></span></td><tr>
						</table>
					  </div>
					</div>
				  </div>
				</div>
			</div>
			
			
			
			<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
			  <div class="modal-dialog" role="document">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Add Lego</h4>
				  </div>
				  <div class="modal-body">
					<form name="myForm">
					  <div class="form-group">
						<label for="exampleInputEmail1">Photo</label>
						<div class="input-group">
							<input class="form-control" type="text" name="legoName" ng-model="photo" required>
							<span class="input-group-btn">
								<button class="btn btn-default" ng-click='browseimage()' type="button">Browse</button>
						    </span>
						</div>
					  </div>
					  <div class="form-group">
						<label for="exampleInputEmail1">Name</label>
						<input class="form-control" type="text" name="legoName" ng-model="name" required>
					  </div>
					  <div class="form-group">
						<label for="exampleInputEmail1">Merk</label>
						<select ng-model="merek" class="form-control">
							<option ng-repeat="x in dMerek" value="{{ x.merek_id }}">{{ x.nama_merek }}</option>
						</select>
					  </div>
					  <div class="form-group">
						<label for="exampleInputEmail1">Series</label>
						<select ng-model="seri" class="form-control">
							<option ng-repeat="z in dSeri" value="{{ z.seri_id }}">{{ z.nama_seri }}</option>
						</select>
					  </div>
					  <div class="form-group">
						<label for="exampleInputEmail1">Pieces</label>
						<input class="form-control" type="number" name="legoName" ng-model="pcs" required>
					  </div>
					  <div class="form-group">
						<label for="exampleInputEmail1">Color</label>
						<input class="form-control" type="color" name="legoName" ng-model="color" required>
					  </div>
					  <div class="form-group">
						<label for="exampleInputEmail1">Buy Date</label>
						<input class="form-control" type="date" name="legoName" ng-model="bdate" required>
					  </div>
					</form>
				  </div>
				  <div class="modal-footer">
					
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button ng-click="addLego()" type="button" class="btn btn-primary">Save</button>
				  </div>
				</div><!-- /.modal-content -->
			  </div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
			
			<div id="myModal2" class="modal fade" tabindex="-1" role="dialog">
			  <div class="modal-dialog" role="document">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Browse Image</h4>
				  </div>
				  <div class="modal-body">
					<div class="row imgx">
					</div>
				  </div>
				  <div class="modal-footer">
					
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button ng-click="addLego()" type="button" class="btn btn-primary">Add</button>
				  </div>
				</div><!-- /.modal-content -->
			  </div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
			
	</div>
<script>
var app = angular.module('myApp', []);
app.controller('customersCtrl', function($scope, $http) {
  $http.get("../display").then(function (response) {
      $scope.myData = response.data.content;
  });
  
  $http.get("../display/merek").then(function (response) {
      $scope.dMerek = response.data.content;
  });
  
  $http.get("../display/seri").then(function (response) {
      $scope.dSeri = response.data.content;
  });
  
  $http.get("../display/getstatistik1").then(function (response) {
      $scope.dStatistik = response.data.content;
  });
  
  $http.get("../display/GetStatistikMerek").then(function (response) {
      $scope.dStatistikMerek = response.data.content;
  });
  
  $scope.showAddLego=function(){
	  
  };
  
  $scope.deleteLego=function(idx){
	  var parameter = JSON.stringify({id:idx});
	  return $http.post("../display/deleteLego",parameter)
				.then(function (response) {
					$http.get("../display").then(function (response) {
						  $scope.myData = response.data.content;
					  });
					return response;
		});
  }
  
  $scope.browseimage=function(){
	  var dataimg;
	  $http.get("../display/getimagefiles").then(function (response) {
		  dataimg=response.data;
		  for(var x in dataimg){
			  $(".imgx").append("<div class='col-md-2'> <a href='#' class='thumbnail'><img src='../uploads/"+dataimg[x]+"' /></a></div>")
		  }
	  });
	  
	  
	 
	 
	$('#myModal2').modal('show');	
  };
  
  $scope.addLego = function (searchString) {
			var parameter = JSON.stringify({
				nama:$scope.name,
				merek:$scope.merek,
				seri:$scope.seri,
				jumlah:$scope.pcs,
				warna:$scope.color,
				tanggal_beli:$scope.bdate,
				url_photo:$scope.photo
			});
			return $http.post("../display/saveLego",parameter)
				.then(function (response) {
					$http.get("../display").then(function (response) {
						  $scope.myData = response.data.content;
					  });
					return response;
			});
		};
  
});
</script>

</body>
</html>

