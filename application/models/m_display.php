<?php 
class M_display extends CI_Model{
	function tampil_data(){
		$this->db->select('*'); // Select field
		$this->db->from('data_lego'); // from Table1
		$this->db->join('data_merek','data_lego.merek = data_merek.merek_id','INNER'); // Join table1 with table2 based on the foreign key
		$this->db->join('data_seri','data_lego.seri = data_seri.seri_id','INNER');
		//$this->db->where('table1.col1',2); // Set Filter
		$this->db->order_by("tanggal_beli", "desc");
		$res = $this->db->get();
		return $res;
	}

	
	function tampil_statistik(){
		$this->db->select('data_seri.nama_seri, SUM(jumlah) as count'); // Select field
		$this->db->from('data_lego'); // from Table1
		$this->db->join('data_seri','data_lego.seri=data_seri.seri_id','INNER');
		$this->db->group_by('seri');
		$res = $this->db->get();
		return $res;
	}
	
	function tampil_statistik_merek(){
		$this->db->select('data_merek.nama_merek, SUM(jumlah) as count'); // Select field
		$this->db->from('data_lego'); // from Table1
		$this->db->join('data_merek','data_lego.merek=data_merek.merek_id','INNER');
		$this->db->group_by('merek');
		$res = $this->db->get();
		return $res;
	}
	
	function tampil_statistik_color(){
		$this->db->select('warna,SUM(jumlah) as count'); // Select field
		$this->db->from('data_lego'); // from Table1
		$this->db->group_by('warna');
		$res = $this->db->get();
		return $res;
	}
	
	function tampil_statistik_year(){
		$this->db->select('YEAR(tanggal_beli) as Tahun,SUM(jumlah) as count'); // Select field
		$this->db->from('data_lego'); // from Table1
		$this->db->group_by('YEAR(tanggal_beli)');
		$res = $this->db->get();
		return $res;
	}
	
	function tampil_merek(){
		$this->db->select('*'); // Select field
		$this->db->from('data_merek'); // from Table1
		$res = $this->db->get();
		return $res;
	}
	
	function tampil_seri(){
		$this->db->select('*'); // Select field
		$this->db->from('data_seri'); // from Table1
		$res = $this->db->get();
		return $res;
	}
	
	 public function insertLegoAPI($dataLego) //API
	{
		  $val = array(
			'nama' => $dataLego['nama'],
			'merek' => $dataLego['merek'],
			'seri' => $dataLego['seri'],
			'jumlah' => $dataLego['jumlah'],
			'warna' => $dataLego['warna'],
			'tanggal_beli' => $dataLego['tanggal_beli'],
			'url_photo' => $dataLego['url_photo']
		  );
		  $this->db->insert('data_lego', $val);
	}
	
	function insertFunct($data,$table){
		$this->db->insert($table,$data);
	}
	
	function deleteFunct($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
	
	public function deleteLego($dataLego)
	{
		$this->db->where('id', $dataLego['id']);
		$this->db->delete('data_lego');
	}
	
	function updateLego($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}	
}
?>