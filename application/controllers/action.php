<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Action extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->model('m_display');
        $this->load->helper(array('form','url'));
		
	}
	
	public function tambah()
	{
		$data['data_seri'] = $this->m_display->tampil_seri()->result();
		$data['data_merek'] = $this->m_display->tampil_merek()->result();
		$data['data_lego'] = $this->m_display->tampil_data()->result();
		$this->m_display->load->view('lego_tambah',$data);
	}
	
	public function image()
	{
		$this->load->helper('directory');
		$map = directory_map('uploads');
		$data['data_images'] = $map;
		$this->m_display->load->view('image_organizer',$data);
	}
	
	public function saveLego(){
		$nama = $this->input->post('nama');
		$merek = $this->input->post('merek');
		$seri = $this->input->post('seri');
		$jumlah = $this->input->post('jumlah');
		$warna = $this->input->post('warna');
		$tanggal_beli = $this->input->post('tanggal_beli');
		$url_photo = $this->input->post('url_photo');
		$id = $this->input->post('id');
		$mode = $this->input->post('mode');
		
		if($mode==1){
			$data = array(
				'nama' => $nama,
				'merek' => $merek,
				'seri' => $seri,
				'jumlah' => $jumlah,
				'warna' => $warna,
				'tanggal_beli' => $tanggal_beli,
				'url_photo' => $url_photo,
			);
			$this->m_display->insertFunct($data,'data_lego');
		}
		else{
			$data = array(
				'nama' => $nama,
				'merek' => $merek,
				'seri' => $seri,
				'jumlah' => $jumlah,
				'warna' => $warna,
				'tanggal_beli' => $tanggal_beli,
				'url_photo' => $url_photo,
			);
			
			$where = array(
				'id' => $id
			);
			
			$this->m_display->updateLego($where,$data,'data_lego');
		}		
		
		$data['data_seri'] = $this->m_display->tampil_seri()->result();
		$data['data_merek'] = $this->m_display->tampil_merek()->result();
		$data['data_lego'] = $this->m_display->tampil_data()->result();
		$data['Success']=true;
		$data['Info']="Lego have Saved !";
		$this->m_display->load->view('lego_tambah',$data);


		
	}
	
	
	public function deleteLego()
	{
		  $id = $this->input->post('id');
		  //echo $id." lalala";
		  $where = array('id' => $id);
		  $this->m_display->deleteFunct($where,'data_lego');
		  redirect('action/tambah');
	}
	
	public function delete_image($name)
	{
		$this->load->helper("file");
		if (!unlink("uploads/".$name))
	    {
				$this->load->helper('directory');
				$map = directory_map('uploads');
				$data['data_images'] = $map;
				$data['Success']=false;
				$data['Info']="Error deleting $name";
				$this->m_display->load->view('image_organizer',$data);
	    }
		else
		{
				$this->load->helper('directory');
				$map = directory_map('uploads');
				$data['data_images'] = $map;
				$data['Success']=true;
				$data['Info']="Deleted $name";
				$this->m_display->load->view('image_organizer',$data);
		}	
	}
	
	public function uploadfile()
        {
                $target_dir = "uploads/";
				$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
				$uploadOk = 1;
				$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
				$succes=true;
				$info="";

				
				if(isset($_POST["submit"])) {
					$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
					if($check !== false) {
						$info="File is an image - " . $check["mime"] . ".";
						$uploadOk = 1;
						$succes=true;
					} else {
						$info="File is not an image.";
						$uploadOk = 0;
						$succes=false;
					}
				}

				if (file_exists($target_file)) {
					$info="Sorry, file already exists.";
					$uploadOk = 0;
					$succes=false;
				}

				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" ) {
					$info="Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
					$succes=false;
				}

				if ($uploadOk == 0) {

				} else {
					if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
						// File and new size
						$filename = "uploads/".$_FILES["fileToUpload"]["name"];
						$percent = 0.5;
						// Get new sizes
						list($width, $height) = getimagesize($filename);
						$newwidth = $width * $percent;
						$newheight = $height * $percent;
						$config['image_library'] = 'gd2';
						$config['source_image'] = $filename;
						$config['create_thumb'] = FALSE;
						$config['maintain_ratio'] = TRUE;
						$config['width']         = 420;
						$config['height']       = 420;
						$this->load->library('image_lib', $config);
						$this->image_lib->resize();
						
						$config['image_library'] = 'gd2';
						$config['source_image'] = $filename;
						$config['create_thumb'] = FALSE;
						$config['wm_text'] = 'Partager.us';
						$config['wm_type'] = 'text';
						$config['wm_font_path'] = './system/fonts/MoonLight.otf';
						$config['wm_font_size'] = '10';
						$config['wm_font_color'] = '000000';
						$config['wm_vrt_alignment'] = 'bottom';
						$config['wm_hor_alignment'] = 'center';
						$config['wm_padding'] = '-10';

						$this->image_lib->initialize($config);

						$this->image_lib->watermark();

					
						$info="The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
					} else {
						//$info="Sorry, there was an error uploading your file.";
					}
				}
				
				$response = array(
				'Success' => $succes,
				'Info' => $info);
				
				$this->load->helper('directory');
				$map = directory_map('uploads');
				$data['data_images'] = $map;
				$data['Success']=$succes;
				$data['Info']=$info;
				$this->m_display->load->view('image_organizer',$data);
				
        }
}
