<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statistik extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->model('m_display');
        $this->load->helper(array('form','url'));
		
	}
	
	
	public function Index(){
		$data['data_s1'] = $this->m_display->tampil_statistik()->result();
		$data['data_s2'] = $this->m_display->tampil_statistik_merek()->result();
		$data['data_s3'] = $this->m_display->tampil_statistik_color()->result();
		$data['data_s4'] = $this->m_display->tampil_statistik_year()->result();
		$this->m_display->load->view('Statistik',$data);

	}

}
