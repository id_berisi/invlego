<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
	
	function __construct(){
		parent::__construct();		
		$this->load->model('m_display');
        $this->load->helper(array('form', 'url'));
		
	}

	public function index(){
		//echo "ini method index pada controller belajar";
		//$data['data_lego'] = $this->m_display->tampil_data()->result();
		//$this->load->view('v_tampil',$data);
		
		$response = array(
			'content' => $this->m_display->tampil_data()->result()
		);
		
		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT))
			->_display();
      exit;
		
	}
	
	public function merek(){
		$response = array(
			'content' => $this->m_display->tampil_merek()->result()
		);
		
		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT))
			->_display();
      exit;
	}
	
	public function GetStatistik1(){
		$response = array(
			'content' => $this->m_display->tampil_statistik()->result()
		);
		
		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT))
			->_display();
      exit;
	}
	
	public function GetStatistikMerek(){
		$response = array(
			'content' => $this->m_display->tampil_statistik_merek()->result()
		);
		
		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT))
			->_display();
      exit;
	}
	
	public function seri(){
		$response = array(
			'content' => $this->m_display->tampil_seri()->result()
		);
		
		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT))
			->_display();
      exit;
	}
	
	public function saveLego()
	{
		  $data = (array)json_decode(file_get_contents('php://input'));
		  $this->m_display->insertLego($data);

		  $response = array(
			'Success' => true,
			'Info' => 'Data Tersimpan');

		  $this->output
			->set_status_header(201)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT))
			->_display();
			exit;
	}
	
	public function deleteLego()
	{
		  $data = (array)json_decode(file_get_contents('php://input'));
		  $this->m_display->deleteLego($data);

		  $response = array(
			'Success' => true,
			'Info' => 'Data Dihapus');

		  $this->output
			->set_status_header(201)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT))
			->_display();
			exit;
	}
	
	public function uploadform()
    {
         $this->load->view('Upload_form', array('error' => ' ' ));
    }
	
	public function getImageFiles(){
		$this->load->helper('directory');
		$map = directory_map('uploads');

		$this->output
			->set_status_header(201)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($map, JSON_PRETTY_PRINT))
			->_display();
			exit;
	}
	
	public function uploadfile()
        {
                $target_dir = "uploads/";
				$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
				$uploadOk = 1;
				$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
				// Check if image file is a actual image or fake image
				if(isset($_POST["submit"])) {
					$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
					if($check !== false) {
						$info="File is an image - " . $check["mime"] . ".";
						$uploadOk = 1;
						$succes=true;
					} else {
						$info="File is not an image.";
						$uploadOk = 0;
						$succes=false;
					}
				}
				// Check if file already exists
				if (file_exists($target_file)) {
					$info="Sorry, file already exists.";
					$uploadOk = 0;
					$succes=false;
				}
				// Check file size
				//if ($_FILES["fileToUpload"]["size"] > 500000) {
					//echo "Sorry, your file is too large.";
					//$uploadOk = 0;
				//}
				// Allow certain file formats
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" ) {
					$info="Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
					$succes=false;
				}
				// Check if $uploadOk is set to 0 by an error
				if ($uploadOk == 0) {
					//$info="Sorry, your file was not uploaded.";
				// if everything is ok, try to upload file
				} else {
					if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
						$info="The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
					} else {
						//$info="Sorry, there was an error uploading your file.";
					}
				}
				
				$response = array(
				'Success' => $succes,
				'Info' => $info);
				
				 $this->output
					->set_status_header(201)
					->set_content_type('application/json', 'utf-8')
					->set_output(json_encode($response, JSON_PRETTY_PRINT))
					->_display();
					exit;
        }


}
?>