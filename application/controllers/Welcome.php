<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->model('m_display');
        $this->load->helper(array('url'));
		
	}
	
	public function index()
	{
		$data['data_lego'] = $this->m_display->tampil_data()->result();
		$this->m_display->load->view('home',$data);
	}
}
