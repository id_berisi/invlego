<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->view('header2'); ?>
</head>
<body>

<div class="container">
		<?php $this->view('breadcrumb',Array('pnum' => 4)); ?>
		<div class="page-header">
		  <?php $this->view('title'); ?>
		</div>
		 <div class='panel bdy'>
		  <div class="row">
			<div class='col-md-4'>
				<table class='table'>
				<tr><th>Merk</th><th>Amount</th></tr>
			<?php 
				foreach($data_s2 as $u){ 
					echo "<tr>";
					echo "<td>".$u->nama_merek."</td>";
					echo "<td>".$u->count."</td>";
					echo "</tr>";
				}
			?>	</table>
			</div>
			<div class='col-md-4'>
				<table class='table'>
				<tr><th>Series</th><th>Amount</th></tr>
			<?php 
				foreach($data_s1 as $u){ 
					echo "<tr>";
					echo "<td>".$u->nama_seri."</td>";
					echo "<td>".$u->count."</td>";
					echo "</tr>";
				}
			?>	</table>
			</div>
			
			<div class='col-md-4'>
				<table class='table'>
				<tr><th>Colors</th><th>Amount</th></tr>
			<?php 
				foreach($data_s3 as $u){ 
					echo "<tr>";
					echo "<td style='background:$u->warna'>".$u->warna."</td>";
					echo "<td>".$u->count."</td>";
					echo "</tr>";
				}
			?>	</table>
			</div>
		  </div>
		  <div class="row">
			<div class='col-md-4'>
				<table class='table'>
				<tr><th>Years</th><th>Amount</th></tr>
			<?php 
				foreach($data_s4 as $u){ 
					echo "<tr>";
					echo "<td>".$u->Tahun."</td>";
					echo "<td>".$u->count."</td>";
					echo "</tr>";
				}
			?>	</table>
			</div>
			
		  </div>
</div>
</div>
</body>
</html>