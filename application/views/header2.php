<?php $this->load->helper('url'); ?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="<?php echo base_url("assets/js/jquery-3.1.1.min.js") ?>"></script>
<script src="<?php echo base_url("assets/js/bootstrap.min.js") ?>"></script>
<script src="<?php echo base_url("assets/js/pace.min.js") ?>"></script>
<link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap.min.css") ?>">
<link rel="stylesheet" href="<?php echo base_url("assets/css/font-awesome.min.css") ?>">
<link href="<?php echo base_url("assets/css/pace.css") ?>" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url("assets/css/style.css") ?>">