<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->view('header'); ?>
</head>
<body>

<div class="container">
		<?php $this->view('breadcrumb',Array('pnum' => 1)); ?>
		<div class="page-header">
		  <?php $this->view('title'); ?> 
		  <?php echo anchor('action/tambah','<i class="fa fa-plus fa-2x"></i>'); ?>
		  <?php echo anchor('statistik','<i class="fa fa-pie-chart fa-2x"></i>'); ?>
		</div>
		 <div class='panel bdy'>
		  <div class="row">
			<?php 
				foreach($data_lego as $u){ 
			?>
			<div class="col-md-3">
					<div class="panel panel-default legobox">
					  <div class="panel-body" style='padding:0'>
						<div class='legoimg' style='background:url(uploads/<?php echo $u->url_photo ?>) no-repeat center;background-size:cover;'></div>
						<table class="table">
							<tr><td colspan="2"><center><?php echo $u->nama ?></center></td><tr>
							<tr><td>Merk</td><td><?php echo $u->nama_merek ?></td><tr>
							<tr><td>Series</td><td><?php echo $u->nama_seri ?></td><tr>
							<tr><td>Buy Date</td><td><?php echo $u->tanggal_beli ?></td><tr>
							<tr><td>Main Color</td><td><span class="glyphicon glyphicon-stop" style="color:<?php echo $u->warna ?>"></td> <tr>
						</table>
					  </div>
					</div>
			</div>
			<?php } ?>
		  </div>
</div>
</div>
</body>
</html>