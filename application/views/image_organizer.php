<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->helper('url');
?><!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->view('header2'); ?>
</head>
<body>

<div class="container">
		  <?php $this->view('breadcrumb',Array('pnum' => 3)); ?>
		  <div class="page-header"><?php $this->view('title'); ?></div>
		  <div class='panel bdy'>
			<?php if(isset($Info) and isset($Success)){
				if ($Success){
					?>
					<div class="alert alert-success alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					  <strong>Success!</strong> <?php echo $Info ?>
					</div>
				<?php
				}
				else{
					?>
					<div class="alert alert-danger alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					  <strong>Success!</strong> <?php echo $Info ?>
					</div>
				<?php	
				}
			} ?>

			<?php echo form_open_multipart('action/uploadfile');?>
				<div class="form-group">
					<label for="exampleInputEmail1">Photo</label>
					<input class="form-control" type="file" name="fileToUpload" ng-model="photo" required>
				</div>
				<div class="form-group">
					<button type="submit" class=" btn btn-primary">Save</button>
				</div>
			</form>

		  <div class="row">
			<?php 
				foreach($data_images as $u){ 
			?>
			<div class="col-md-2" >
				<div class="thumbnail" >
				  <img style='height:150px' src='<?php echo base_url("uploads/$u") ?>' alt="...">
				  <div class="caption">
					<p><?php echo $u ?></p>
					<?php echo anchor('action/delete_image/'.$u,"<p><input type='button' class='form-control btn btn-danger' value='delete' /></p>"); ?>	
				  </div>
				</div>
			</div>
			<?php } ?>
		  </div>
		</div>
		</div>
		
		<script>
		  function copyToClipboard(text) {
			window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
		  }
</script>
</body>
</html>