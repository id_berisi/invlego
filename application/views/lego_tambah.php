<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->view('header2'); ?>
	<script src="<?php echo base_url("assets/js/sweetalert.min.js") ?>"></script>
	<script src="<?php echo base_url("assets/js/dataTables.min.js") ?>"></script>
	<link rel="stylesheet" href="<?php echo base_url("assets/css/dataTables.min.css") ?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/css/style.css") ?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/css/sweetalert.css") ?>">
	
	<script>
		var baseurl="<?php echo base_url() ?>"
		var url="<?php echo base_url("api/getImageFiles") ?>";
		$(document).ready(function(){		
			$('#legtbl').DataTable();
			$('.browseimage').click(function(){
				$.getJSON( url, function( data ) {
				  var items = [];
				  $.each( data, function( key, val ) {
					items.push( "<div class='col-md-3 col-xs-4'><div class='panel'><img class='imgsl' val='"+val+"' style='width:100%' src='"+baseurl+"uploads/"+val+"'/></div></div>" );
				  });
				 
				  $( "<div/>", {
					"class": "my-new-list",
					html: items.join( "" )
				  }).appendTo( "#imgs" );
				});
				$('#imagelist').modal('show');
			});
			
			$(".updtl").click(function(){
				var datas=$(this).attr('atx');
				var data=datas.split(",");
				$("#mode").val(2);
				$("#id").val(data[0]);
				$("#url_photo").val(data[7]);
				$("#nama").val(data[1]);
				$("#merek").val(data[2]);
				$("#seri").val(data[3]);
				$("#jumlah").val(data[4]);
				$("#warna").val(data[5]);
				$("#tanggal_beli").val(data[6]);
			});
		});
	</script>
</head>
<body>

<div class="container">
			<?php $this->view('breadcrumb',Array('pnum' => 2)); ?>
			<div class="page-header"><?php $this->view('title'); ?> 
				<a href="<?php echo base_url("action/image") ?>" class="fa fa-camera fa-2x" aria-hidden="true"> </a> 
				<a href="#"  class="fa fa-cubes fa-2x" aria-hidden="true"> </a> 
				<a href="#"  class="fa fa-th-large fa-2x" aria-hidden="true"> </a>
			</div>
				<div class='panel'>
				<?php if(isset($Info) and isset($Success)){
					if ($Success){
						?>
						<div class="alert alert-success alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  <strong>Success!</strong> <?php echo $Info ?>
						</div>
					<?php
					}
					else{
						?>
						<div class="alert alert-danger alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  <strong>Fail!</strong> <?php echo $Info ?>
						</div>
					<?php	
					}
				} ?>
					  
			  <div class="row">
				<div class='col-md-6'>
					<form action="<?php echo base_url(). 'action/saveLego'; ?>" method="post">
						  <input class="form-control" type="hidden" id="mode" name="mode" value="1">
						  <input class="form-control" type="hidden" id="id" name="id" value="1">
						  <div class="form-group">
							<div class="input-group">
								<input class="form-control" type="text" id="url_photo" name="url_photo" required>
								<span class="input-group-btn">
									<a  class="btn btn-default browseimage">Browse</a>
								</span>
							</div>
						  </div>
						  <div class="form-group">
							<label for="exampleInputEmail1">Name</label>
							<input class="form-control" type="text" id="nama" name="nama" ng-model="name" required>
						  </div>
						  <div class="form-group">
							<label for="exampleInputEmail1">Merk</label>
							<select name="merek" id="merek" class="form-control">
								<?php 
									foreach($data_merek as $u){ 
										echo "<option value='$u->merek_id'>".$u->nama_merek."</option>";
									};
								?>
							</select>
						  </div>
						  <div class="form-group">
							<label for="exampleInputEmail1">Series</label>
							<select name="seri" id="seri" class="form-control">
								<?php 
									foreach($data_seri as $u){ 
										echo "<option value='$u->seri_id'>".$u->nama_seri."</option>";
									};
								?>
							</select>
						  </div>
						  <div class="form-group">
							<label for="exampleInputEmail1">Pieces</label>
							<input class="form-control" id="jumlah" type="number" name="jumlah" ng-model="pcs" required>
						  </div>
						  <div class="form-group">
							<label for="exampleInputEmail1">Color</label>
							<input class="form-control" id="warna" type="color" name="warna" ng-model="color" required>
						  </div>
						  <div class="form-group">
							<label for="exampleInputEmail1">Buy Date</label>
							<input class="form-control" id="tanggal_beli" type="date" name="tanggal_beli" ng-model="bdate" required>
						  </div>
						  
						  <div class="form-group">
							<button type="submit" class="btn btn-primary">Save</button>
						  </div>
						  
						  
						</form>
				</div>
				<div class='col-md-6'>
					<table id="legtbl" class='table'>
						<thead><tr><th>Name</th><th>Merek</th><th>Seri</th><th></th><th></th></tr></thead>
						<?php 
							foreach($data_lego as $u){
							$url=base_url(). 'action/deleteLego';
							echo "<tr><td>$u->nama</td><td>$u->nama_merek</td><td>$u->nama_seri</td><td><form action='$url' method='post'><input type='hidden' name='id' value='$u->id'/><input class='deleteLego btn btn-danger' type='button' value='delete'/></form></td><td><input atx='$u->id,$u->nama,$u->merek_id,$u->seri_id,$u->jumlah,$u->warna,$u->tanggal_beli,$u->url_photo' idl='$u->id' class='updtl btn btn-success' type='button' value='edit'/></td></tr>";
							};
						?>
					</table>
				</div>
			  </div>
			</div>
		</div>
		</div>
		
		<div class="modal fade" id="imagelist" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Browse Image</h4>
			  </div>
			  <div class="modal-body" >
				<div class='row' id="imgs">
				</div>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>
		  </div>
		</div>
		<script>
			$("body").on("click",".imgsl",function(){
				$('#url_photo').val($(this).attr('val'));
				$('#imagelist').modal('toggle');
			});
			
			$("body").on("click",".deleteLego",function(){	
				var formdel=$(this).parent();
				swal({
				  title: "Are you sure?",
				  text: "You will not be able to recover this data!",
				  type: "warning",
				  showCancelButton: true,
				  confirmButtonColor: "#DD6B55",
				  confirmButtonText: "Yes, delete it!",
				  closeOnConfirm: false
				},
				function(){
				  formdel.submit();
				});
			
			});
			
			$("body").on("click",".updtl",function(){			
				var datas=$(this).attr('atx');
				var data=datas.split(",");
				$("#mode").val(2);
				$("#id").val(data[0]);
				$("#url_photo").val(data[7]);
				$("#nama").val(data[1]);
				$("#merek").val(data[2]);
				$("#seri").val(data[3]);
				$("#jumlah").val(data[4]);
				$("#warna").val(data[5]);
				$("#tanggal_beli").val(data[6]);
			});
						
		</script>
</body>
</html>